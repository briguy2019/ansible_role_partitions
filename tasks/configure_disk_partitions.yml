- name: get partitions
  parted:
    device:  "/dev/{{ item.disk }}"
    unit: GiB
  register: info
  with_items: "{{ disk_devices }}"

- name: ensure lvm package is installed
  yum:
    name: lvm2
    state: installed

- name: generate disks and partition tables
  set_fact:
    disk_table: "{{ disk_table|default([]) + [{'disk' : item.item.disk, 'partition_end' : item.partitions[-1].end|int if item.partitions|length > 0 else '0'}] }}"
  when: item.partitions is defined
  with_items: "{{ info.results }}"

#- name: test
#  debug:
#    msg: "{{ item.partitions[-1] }}"
#  with_items:  "{{ info.results }}"

#- name: test
#  debug:
#    msg: "{% for disks  in disk_table %} {% if disks.disk == 'sda' %}{{disks.partition_end}}{% endif %}{% endfor%}"
#  with_items:  "{{ disk_table }}"

#- name: initialize disks
#  shell: "parted /dev/{{ item.0.disk }} mklabel gpt"
#  when: item.0.partition_disk == True and item.0.wipe_disk == False and item.1.partition_type == "linux"
#  with_subelements: 
#    - "{{ disk_devices }}"
#    - partitions
- name: partition linux disks
  parted:
    device: "/dev/{{ item.0.disk }}"
    number: "{{ item.1.partition_number }}"
    state: present
    part_start: "{% for disks  in disk_table %}{% if disks.disk == item.0.disk %}{{ disks.partition_end }}{{ '0%' if disks.partition_end == '0' else 'GiB' }}{%endif%}{%endfor%}"
    part_end: "{% for disks  in disk_table %}{% if disks.disk == item.0.disk %}{{ disks.partition_end | int + item.1.size_gb if item.1.size_gb != '100%' else '100%' }}{{ 'GiB' if item.1.size_gb | string != '100%' else '' }}{%endif%}{%endfor%}"
    label: "{{ item.1.disk_label }}"
    name: "{{ item.1.disk_partition_name }}"
  when: item.0.partition_disk and not item.0.wipe_disk and item.1.partition_type == "linux"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions

- name: partition linux disks for lvm
  parted:
    device: "/dev/{{ item.0.disk }}"
    number: "{{ item.1.partition_number }}"
    state: present
    part_start: "{% for disks  in disk_table %}{% if disks.disk == item.0.disk %}{{ disks.partition_end }}{{ '0%' if disks.partition_end == '0' else 'GiB' }}{%endif%}{%endfor%}"
    part_end: "{% for disks  in disk_table %}{% if disks.disk == item.0.disk %}{{'100%'}}{%endif%}{%endfor%}"
    label: "{{ item.1.disk_label }}"
    name: "{{ item.0.vg_name }}"
  when: item.0.partition_disk and not item.0.wipe_disk and item.1.partition_type == "lvm" and item.0.disk.find('loop') == -1
  with_subelements:
    - "{{ disk_devices }}"
    - partitions


 
#- name: wipe partitions
#  shell: "sgdisk -Z /dev/{{  item.0.disk }}{{ item.1.partition_number }}"
#  when: item.1.format_disk == True and item.0.wipe_disk == False and item.1.partition_type == "linux"
#  with_subelements: 
#    - "{{ disk_devices }}"
#    - partitions



- name: lvm create volume group
  lvg:
    vg: "{{ item.0.vg_name }}"
    pvs: "/dev/{{ item.0.disk }}{{ item.1.partition_number if item.0.disk.find('loop') == -1 else '' }}"
  when: item.0.partition_disk and not item.0.wipe_disk and item.1.partition_type == "lvm"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions

- name: lvm create logical volume
  lvol:
    vg: "{{ item.0.vg_name }}"
    lv: "{{ item.1.disk_partition_name }}"
    size: "{{ item.1.size_gb if item.1.size_gb != '100%' else '+100%' }}{{ 'g' if item.1.size_gb | string != '100%' else 'FREE' }}"
  when: item.0.partition_disk and not item.0.wipe_disk and item.1.partition_type == "lvm"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions


- name: Create filesystem on disks
  filesystem:
    fstype: "{{ item.1.fstype }}"
    force: no
    opts: "{{ item.1.fs_options | default }}"
    dev: "/dev/{{  item.0.disk }}{{ item.1.partition_number }}"
  when: item.1.format_disk and not item.0.wipe_disk and item.1.partition_type == "linux"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions

- name: Create filesystem on lvm volumes
  filesystem:
    fstype: "{{ item.1.fstype }}"
    force: no
    opts: "{{ item.1.fs_options | default }}"
    dev: "/dev/{{  item.0.vg_name }}/{{ item.1.disk_partition_name }}"
    resizefs: true
  when: item.1.format_disk and not item.0.wipe_disk and item.1.partition_type == "lvm"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions

- name: Create mount points
  file:
    path: "{{ item.1.mountpoint }}"
    state: directory
  when: item.1.format_disk and not item.0.wipe_disk and item.1.mountpoint is defined
  with_subelements:
    - "{{ disk_devices }}"
    - partitions

- name: Get UUIDs of partitions
  shell: |
    set -o pipefail
    /sbin/blkid /dev/{{ item.0.disk }}{{ item.1.partition_number }} -o value | head -n1
  when: item.1.mountpoint is defined and not item.0.wipe_disk and item.1.partition_type == "linux"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions
  register: blk_uuids

- name: Mount linux partitions
  mount:
    path: "{{ item.item[1].mountpoint }}"
    src: "UUID={{ item.stdout }}"
    fstype: "{{ item.item[1].fstype }}"
    state: mounted
  when: not item.item[0].wipe_disk and item.item[1].mountpoint is defined and item.item[1].partition_type == "linux"
  with_items: "{{ blk_uuids.results }}"


- name: Mount lvm volumes
  mount:
    path: "{{ item.1.mountpoint }}"
    src: "/dev/{{  item.0.vg_name }}/{{ item.1.disk_partition_name }}"
    fstype: "{{ item.1.fstype }}"
    state: mounted
  when: item.1.mountpoint is defined and not item.0.wipe_disk and item.1.partition_type == "lvm"
  with_subelements:
    - "{{ disk_devices }}"
    - partitions
