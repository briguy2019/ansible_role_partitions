About
-----

Ansible role to setup disk partitions and mountpoints on a linux system.

Variables
---------

disk_devices:

Usage Example
-------------

```yml
vars:
disk_devices:
- disk: vdb
  vg_name: localdata1
  wipe_disk: False
  partition_disk: True
  partitions:
    - disk_partition_name: localdata1
      partition_type: lvm
      size_gb: "100%"
      disk_label: gpt
      fs_options:
      fstype: xfs
      format_disk: True
      partition_number: 1
      mountpoint: "/localdata1"
- disk: vdc
  vg_name: localdata2
  wipe_disk: False
  partition_disk: True
  partitions:
    - disk_partition_name: localdata2
      partition_type: lvm
      size_gb: "14"
      disk_label: gpt
      fs_options:
      fstype: xfs
      format_disk: True
      partition_number: 1
      mountpoint: "/localdata2"

roles:
  - { role: disk_partitions, tags: configure-diskpart }
```

Testing
-------

```bash
sudo ansible-playbook tests/playbook.yml -i tests/inventory
```
Author Information
------------------
Brian Murrill
